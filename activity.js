db.fruits.insertMany([
    {
      name: "Banana",
      supplier: "Farmer Fruits Inc.",
      stocks: 30,
      price: 20,
      onSale: true,
    },
    {
      name: "Mango",
      supplier: "Mango Magic Inc.",
      stocks: 50,
      price: 70,
      onSale: true,
    },
    {
      name: "Dragon Fruit",
      supplier: "Farmer Fruits Inc.",
      stocks: 10,
      price: 60,
      onSale: true,
    },
  ]);
  
  //count operator total number of fruits on sale
  db.fruits.aggregate([
    { $match: { onSale: true } },
    { $count: "fruitsOnSale" },
  ]);
  
  //count operator total number of fruits with stock more than 20
  db.fruits.aggregate([
    { $match: { stocks: { $gt: 20 } } },
    { $count: "fruitsMoreThan20" },
  ]);
  
  //avg operator avg price of fruits on sale
  db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "$supplier", avgPriceOnSale: { $avg: "$price" } } },
  ]);
  
  //max operator highest price per supplier
  db.fruits.aggregate([
    { $group: { _id: "$supplier", highestPrice: { $max: "$price" } } },
  ]);
  
  //min operator lowest price per supplier
  db.fruits.aggregate([
    { $group: { _id: "$supplier", lowestPrice: { $min: "$price" } } },
  ]);
  