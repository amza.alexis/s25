db.course_bookings.insertMany([
    {
        "courseId": "C001",
        "studentId": "S004",
        "isCompleted": true
    },
    {
        "courseId": "C002",
        "studentId": "S001",
        "isCompleted": false
    },
    {
        "courseId": "C001",
        "studentId": "S003",
        "isCompleted": true
    },
    {
        "courseId": "C003",
        "studentId": "S002",
        "isCompleted": false
    },
    {
        "courseId": "C001",
        "studentId": "S002",
        "isCompleted": true
    },
    {
        "courseId": "C004",
        "studentId": "S003",
        "isCompleted": false
    },
    {
        "courseId": "C002",
        "studentId": "S004",
        "isCompleted": true
    },
    {
        "courseId": "C003",
        "studentId": "S007",
        "isCompleted": false
    },
    {
        "courseId": "C001",
        "studentId": "S005",
        "isCompleted": true
    },
    {
        "courseId": "C004",
        "studentId": "S008",
        "isCompleted": false
    },
    {
        "courseId": "C001",
        "studentId": "S013",
        "isCompleted": true
    },
]);

// Aggregation in MongoDB
//This is the act or process of generating manipulated data and perform operations to create filtered results that helps in analyzing data.

//This helps in creating reports from analyzing the data provided in our documents

// Aggregation Pipeline Syntax:

    db.collections.aggregate([
            {Stage 1},
            {Stage 2},
            {Stage 3}
        ]);

// Aggregation Pipelines
// Aggregations is done 2-3 steps typically. The first pipeline was with the use of $match.

// $match is used to pass the documents or get the documents that will match our condition.

    // Syntax:
        {$match: {field:value}}

// $group is used to group elements/documents together and create an analysis of these grouped documents.

    // Syntax:
        {$group: {_id: <id>, fieldResult: "valueResult"}}

// Count all the documents in our collection.

db.course_bookings.aggregate([
        {$group: {_id: null, count:{$sum: 1}}}
    ])

db.course_bookings.aggregate([
            {$group: {_id: "$courseId", count:{$sum: 1}}}
        ])


db.course_bookings.aggregate([
        {$match: {"isCompleted": true}},
        {$group: {_id: "$courseId", count:{$sum: 1}}}
    ]);

db.course_bookings.aggregate([
        {$match: {"isCompleted" : true}},
        {$project: {"courseId": 0}}
    ])

db.course_bookings.aggregate([
        {$match: {"isCompleted": true}},
        {$sort: {"courseId": -1}}
    ])

/*
    1 = ascending
    -1 = descending
*/

/*Mini-Activity:
    a. Count the completed courses of student S013
*/

db.course_bookings.aggregate([
        {$match: {"isCompleted": true}},
        {$match: {"studentId": "S013"}},
        {$group: {_id: "$studentId", count:{$sum: 1}}}
    ])


db.orders.insertMany([
        {
                "cust_Id": "A123",
                "amount": 500,
                "status": "A"
        },
        {
                "cust_Id": "A123",
                "amount": 250,
                "status": "A"
        },
        {
                "cust_Id": "B212",
                "amount": 200,
                "status": "A"
        },
        {
                "cust_Id": "B212",
                "amount": 200,
                "status": "D"
        }
])

db.orders.aggregate([
        {$match: {"status": "A"},{"status": "D"}},	
        {$group: {_id: "$cust_Id", total: {$sum: "$amount"}}}
    ])

    // $sum operator will total the values
    // $avg operator will average the results
    // $max operator will show you the highest value
    // $min operator will show you the lowest value